﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment2
{
    public class Clothes
    {
        //Creating a static variable
        public static int size;

        //creating a static method
        public static void PrintSize(int size)
        {
            Console.WriteLine("Size is : {0}", size);
            Console.ReadLine();
        }
    }

    public abstract class Food
    {
        //Creating abstract method
        public abstract void CookFood();
    }

    // Inherting the abstract class
    public class Breakfast: Food
    {
        //overriding the abstract method created in abstract class
        public override void CookFood()
        {
            Console.WriteLine("Breakfast is cooked");
            Console.ReadLine();
        }
    }


    //creating an interface
    public interface IVehicle
    {
        void VehicleSound();
    }

    //Inheriting and implementing interface
    public class Car: IVehicle
    {
        //Declaring a private field
        private string CarName;

        //setter for the private field
        public void SetCarName(string cName)
        {
            CarName = cName;
        }

        //getter for the private field
        public string GetCarName()
        {
            return CarName;
        }

        public void PrintCarName()
        {
            Console.WriteLine("The car name is {0}", CarName);
            Console.ReadLine();
        }

        public void PrintCarName(string carName)
        {
            Console.WriteLine("The passed car name is {0}", carName);
            Console.ReadLine();
        }

        public void VehicleSound()
        {
            Console.WriteLine("This is car sound");
            Console.ReadLine();
        }
    }



    internal class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Enter size of the cloth");
            int n = Convert.ToInt32(Console.ReadLine());

            // Accessing and assigning the value of static variable from other class
            Clothes.size = n;
            Console.WriteLine(Clothes.size);

            //Accessing and calling the static method
            Clothes.PrintSize(n);


            //calling the abstract method through its child class
            Breakfast bf = new Breakfast();
            bf.CookFood();


            //Calling and implementing the interface through its child class
            Car carObj = new Car();
            carObj.VehicleSound();

            //Setting the value of private variable with setter method
            carObj.SetCarName("Lamborghini");

            //Calling the method with no parameter
            carObj.PrintCarName();

            //Calling the method with parameter
            carObj.PrintCarName("Ferrari");


        }
    }
}
